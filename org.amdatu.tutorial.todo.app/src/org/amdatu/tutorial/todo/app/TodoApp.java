/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tutorial.todo.app;

import java.util.Collections;
import java.util.Set;

import javax.ws.rs.core.Application;

import org.amdatu.web.rest.jaxrs.AmdatuWebRestConstants;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.osgi.service.http.whiteboard.HttpWhiteboardConstants;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

import com.fasterxml.jackson.jaxrs.json.JacksonJsonProvider;

@Component(provides = Application.class)
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PATTERN, value = "/*")
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_RESOURCE_PREFIX, value = "/web")
@Property(name = HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_SELECT, value = TodoApp.CONTEXT_SELECT)

@Property(name = JaxrsWhiteboardConstants.JAX_RS_NAME, value = TodoApp.JAX_RS_APPLICATION_NAME)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_APPLICATION_BASE, value = "/rest")
@Property(name = AmdatuWebRestConstants.JAX_RS_APPLICATION_CONTEXT, value = TodoApp.CONTEXT_NAME)
public class TodoApp extends Application {

    public static final String JAX_RS_APPLICATION_NAME = "org.amdatu.tutorial.todo.app";

    public static final String CONTEXT_NAME = "org.amdatu.tutorial.todo.app";

    public static final String CONTEXT_SELECT =
                    "(" + HttpWhiteboardConstants.HTTP_WHITEBOARD_CONTEXT_NAME + "=" + CONTEXT_NAME + ")";

    private Set<Object> singletons;

    public TodoApp() {
        singletons = Collections.singleton(new JacksonJsonProvider());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }

}
