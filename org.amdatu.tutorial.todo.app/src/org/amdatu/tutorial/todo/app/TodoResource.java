/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tutorial.todo.app;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.amdatu.tutorial.todo.api.Todo;
import org.amdatu.tutorial.todo.api.TodoService;
import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Property;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.osgi.service.jaxrs.whiteboard.JaxrsWhiteboardConstants;

@Component(provides = Object.class)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_RESOURCE, booleanValue = true)
@Property(name = JaxrsWhiteboardConstants.JAX_RS_APPLICATION_SELECT, value = TodoApp.JAX_RS_APPLICATION_NAME)
@Path("/")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class TodoResource {

    @ServiceDependency
    private volatile TodoService todoService;

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{user}")
    public List<Todo> list(@PathParam("user") String user) {
      return todoService.list(user);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public void saveTodo(Todo todo) {
      todoService.store(todo);
    }

}
