/**
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.tutorial.todo.memory.test;

import static org.junit.Assert.assertEquals;

import java.util.List;

import org.amdatu.testing.configurator.TestConfigurator;
import org.amdatu.tutorial.todo.api.Todo;
import org.amdatu.tutorial.todo.api.TodoService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class TodoServiceTest {

    private volatile TodoService todoService;

    @Before
    public void before() {
        TestConfigurator.configure(this)
            .add(TestConfigurator.createServiceDependency().setService(TodoService.class).setRequired(true))
            .apply();
    }

    @After
    public void after() {
        TestConfigurator.cleanUp(this);
    }

    @Test
    public void test() {
        todoService.store(new Todo("Test todo 1", "amdatu"));
        todoService.store(new Todo("Test todo 2", "amdatu"));
        todoService.store(new Todo("Test todo 3", "other"));

        List<Todo> list = todoService.list("amdatu");
        assertEquals(2, list.size());
    }

}
