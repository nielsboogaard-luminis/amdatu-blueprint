:toc: left
:toclevels: 3
:toc-title: Amdatu Blueprint
:homepage: https://www.amdatu.org
:source-highlighter: coderay
:icons: font
:docinfo: shared
:blueprint-release: local

= Amdatu Blueprint


include::_includes/intro.adoc[leveloffset=+1]

include::_includes/gettingStarted.adoc[leveloffset=+1]

include::_includes/features.adoc[leveloffset=+1]

include::_includes/workspace.adoc[leveloffset=+1]

include::_includes/resources.adoc[]