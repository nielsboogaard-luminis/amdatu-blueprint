:source-highlighter: coderay
:icons: font

= Workspace

== Using features

To enable additional features for a project use the `-buildfeatures` instruction this will add the api's included in the feature in the project's build path.
The `-runfeatures` instruction is used to add all the bundles required at runtime to a project.

[TIP]
====
For most projects you only need to add buid features, run features are only needed for run configurations and integration test projects.
====

=== Mongodb example

A few examples how the bnd descriptors what the different kinds of project's bnd descriptors look like using blueprint features.

==== Api

An api project often doesn't need any additional features.

.example.api/bnd.bnd
[source]
----
Bundle-Version: 1.0.0
Export-Package: example.api
----

==== Implementation

An implementation project using mongodb, only build features are used.

.example.impl/bnd.bnd
[source]
----
Bundle-Version: 1.0.0
Private-Package: example.impl

-buildfeatures: mongodb
-buildpath: \
    example.api;version=latest
----

==== Integration test

An integration test for the mongodb implementation, the only project type using build and run features

.example.itest/bnd.bnd
[source]
----
Test-Cases: ${classes;CONCRETE;ANNOTATED;org.junit.runner.RunWith}
Private-Package: example.itest

-buildfeatures: mongodb, testing
-buildpath: \
    example.api;version=latest

-runfw: org.apache.felix.framework
-runfeatures: mongodb, web
-runbundles: \
    example.api;version=latest
    example.impl;version=latest
----

==== Run

A run descriptor to run the project (no build features)

.run/example.bndrun
[source]
----
-runfw: org.apache.felix.framework
-runfeatures: mongodb
-runbundles: \
    example.api;version=latest
    example.impl;version=latest
----


== Default features

The Amdatu Blueprint base feature is enabled by default, based on your preferences it's possible enable more features by default.
To make a feature default in a workspace add the update the `cnf/build.bnd` file and add the a comma separated list of features as if it were a project's `bnd.bnd` file.


[source]
.cnf/build.bnd
----
# Default features (turned on for all projects)
-buildfeatures.default: mongodb, web
-runbundles.default: mongodb, web
----

[TIP]
====
This won't disable the features enabled by default in the blueprint, so only additional features need to be added.
====

== Adding third party dependencies

The features provided by Amdatu Blueprint will get you started but at some point you'll most likely need additional dependencies.

=== Adding bundles

To add additional bundles to your workspace add the maven GAV to the `project-deps.maven` file in the workspace configuration project (`cnf`).

For example if you want to query Mongodb using Jongo add the following to the `project-deps.maven` file

[source]
.cnf/project-deps.maven
----
org.jongo:jongo:1.3.0
----

[NOTE]
====
The `project-deps.maven` file is the input for a bnd maven repository, for more information on this refer to the http://bnd.bndtools.org/plugins/maven.html[bnd documentation^]
====

=== Using bundles

Once added the bundles can be used in the `-buildpath` and `-runbundles` instructions of your projects and bnd descriptors.

For the example implementation used in <<Mongodb example>> the bnd descriptor using would become

.example.impl/bnd.bnd
[source]
----
Bundle-Version: 1.0.0
Private-Package: example.impl

-buildfeatures: mongodb
-buildpath: \
    example.api;version=latest,\
    org.jongo
----

=== Extending features

It's also possible to add an additional bundle to a feature to have it always available when a certain feature is used.
For this the we can add some instructions to the `build.bnd` file in the workspace configuration project (`cnf`).


[source]
.cnf/build.bnd
----
-buildpath.mongodb: \
 	${if;(buildfeaturesMerged[]=mongodb); \
		org.jongo\
	}

-runbundles.mongodb: \
	${if;(runfeaturesMerged[]=mongodb); \
		org.jongo\
	}
----

From now on Jongo can be used when the mongodb feature is enabled, without adding it to the project build path and / or runbundles

== Exporting an executable jar

It's likely you want to run your code in some other place than your own machine.
For this a run descriptor can be exported to an executable jar using the Gradle build by running `./gradlew export`.
The executable jar will be stored in the `generated/distributions/executable/` folder of the project containing the bndrun file.

[TIP]
====
The `export` task will create en executable jar for all run descriptors in the workspace.
To generate a single executable jar use `./gradlew :<project>:export.<run descriotor name>`.
Where `<project>` is the name of the project that contains the run descriptor and `<run descriotor name>` is the name of the bndrun file without extension.
====

== Creating a Docker image

Instead of <<Exporting an executable jar>> it's also possible to create a docker image from a run descriptor by adding just a few lines to it.

[source]
.run/run.bndrun
----
... snip ..

# Docker
docker-from: openjdk:8-jre-slim
docker-image: amdatu-blueprint/todo
docker-tags: latest
docker-maintainer: example-maintainer@amdatu.org
docker-expose: 8080
----

Once added a Docker image can be created by running `./gradlew docker`.
Java memory settings can be customized by overriding hte `JAVA_MEMORY` environment variable when starting the Docker container, additional java options can be passed to the continer by configuring the `JAVA_OPTS` environment variable.

[cols="15%,85%"]
.Docker environment variables
|===
| JAVA_OPTS
 | _empty_
| JAVA_MEMORY
 | `-XX:+UnlockExperimentalVMOptions -XX:+UseCGroupMemoryLimitForHeap -XX:MaxRAMFraction=2`
|===

=== Docker instruction overview

[cols="20%,10%,20%,50%"]
|===
| instruction | required | default | description

| docker-from
 | no
 | openjdk-slim-8
 | Base image used for the Docker image

| docker-image
 | *yes*
 | _none_
 | Name of the image

| docker-tags
 | no
 | latest
 | Docker image tag

| docker-maintainer
 | no
 | _none_
 | Docker MAINTAINER (will be omitted if not set)

| docker-expose
 | no
 | 8080
 | Comma separated list of ports to expose will be translated to EXPOSE instructions in the Dockerfile.

| docker-resources
 | no
 | _none_
 | Comma separated list of resources to include in the docker image. Similar to the `-includeresource` instruction (example: `docker-resources: target=source`)

|===

=== Pushing to Docker hub


By running the `dockerPush` task the image will be pushed to Docker Hub.

[source]
----
./gradlew dockerPush
----

To push the image to Docker hub make configure your credentials using environment variables or from gradle.properties.

[cols=2]
|===
| Property
 | environment variable
 | gradle property

| Docker user name
 | DOCKER_USER
 | docker.user
| Docker email
 | DOCKER_EMAIL
 | docker.email
| Docker password
 | DOCKER_PASSWORD
 | docker.password
|===
